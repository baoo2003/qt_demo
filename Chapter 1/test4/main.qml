import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    id: window
    visible: true
    title: qsTr("Login Form")

    Rectangle {
        id: rectangle
        visible: true
        anchors.fill: parent

        Image {
            id: image
            anchors.fill: parent
            source: "background.png"
            fillMode: Image.PreserveAspectFit

            Rectangle {
                id: rectangle1
                x: 257
                width: 800
                height: 50
                color: "#805bcce9"
                radius: 0
                border.color: "#80000000"
                anchors.top: parent.top
                anchors.horizontalCenterOffset: 0
                anchors.topMargin: 0
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: text1
                    width: 254
                    color: "#fdfdfd"
                    text: "Monday, 10-2015 3:14  PM"
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.pixelSize: 18
                    verticalAlignment: Text.AlignVCenter
                    anchors.leftMargin: 0
                    anchors.bottomMargin: 0
                    anchors.topMargin: 0
                }

                MouseArea {
                    id: mouseArea
                    x: 744
                    y: 0
                    width: 50
                    height: 50
                    anchors.right: parent.right
                    anchors.rightMargin: 6

                    Image {
                        id: shutdown_icon
                        anchors.fill: parent
                        source: "shutdown_icon.png"
                        fillMode: Image.PreserveAspectFit
                    }
                }

                MouseArea {
                    id: mouseArea1
                    x: 688
                    y: 0
                    width: 50
                    height: 50
                    anchors.right: parent.right
                    anchors.rightMargin: 62

                    Image {
                        id: restart_icon
                        anchors.fill: parent
                        source: "restart_icon.png"
                        fillMode: Image.PreserveAspectFit
                    }
                }
            }

            Rectangle {
                id: rectangle2
                x: 288
                y: 223
                width: 360
                height: 200
                color: "#80000000"
                radius: 15
                border.color: "#80000000"
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: 100
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: text2
                    x: 41
                    y: 43
                    width: 70
                    height: 20
                    color: "#ffffff"
                    text: qsTr("Username")
                    font.pixelSize: 15
                    font.styleName: "đậm"
                }

                Text {
                    id: text3
                    x: 41
                    y: 84
                    width: 70
                    height: 20
                    color: "#ffffff"
                    text: qsTr("Password")
                    font.pixelSize: 15
                    font.styleName: "đậm"
                }

                Rectangle {
                    id: rectangle3
                    x: 135
                    width: 200
                    height: 20
                    color: "#ffffff"
                    radius: 5
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 43
                    anchors.rightMargin: 25

                    TextInput {
                        id: textInput
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                }

                Rectangle {
                    id: rectangle4
                    x: 135
                    y: 84
                    width: 200
                    height: 20
                    color: "#ffffff"
                    radius: 5
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 96
                    anchors.rightMargin: 25

                    TextInput {
                        id: textInput1
                        anchors.fill: parent
                        font.pixelSize: 12
                    }
                }

                Rectangle {
                    id: rectangle5
                    x: 80
                    y: 139
                    width: 200
                    height: 20
                    color: "#27abc9"
                    radius: 5

                    MouseArea {
                        id: mouseArea2
                        anchors.fill: parent

                        Text {
                            id: text4
                            color: "#ffffff"
                            text: qsTr("Login")
                            anchors.left: parent.left
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            font.pixelSize: 15
                            horizontalAlignment: Text.AlignHCenter
                            font.styleName: "đậm"
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }
                }
            }

            Image {
                id: image3
                x: 319
                y: 75
                width: 200
                height: 200
                source: "logo.png"
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.PreserveAspectFit
            }
        }
    }
}



/*##^##
Designer {
    D{i:0;autoSize:true;height:600;width:800}
}
##^##*/
